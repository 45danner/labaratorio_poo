"""Definir las clases Punto que representa a un punto en el plano y Punto3D,
que represente un punto en el espacio. Esta nueva clase contendrá los mismos métodos de Punto, pero para tres coordenadas."""

import math

class Punto:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def distancia(self, otro_punto):
        return math.sqrt((otro_punto.x - self.x)**2 + (otro_punto.y - self.y)**2)
    
    def __str__(self):
        return "({}, {})".format(self.x, self.y)
    
class Punto3D(Punto):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z
    
    def distancia(self, otro_punto):
        return math.sqrt((otro_punto.x - self.x)**2 + (otro_punto.y - self.y)**2 + (otro_punto.z - self.z)**2)
    
    def __str__(self):
        return "({}, {}, {})".format(self.x, self.y, self.z)
p = Punto(3, 4)

x = p.get_x()
y = p.get_y()

print(f"Coordenadas del punto: ({x}, {y})")

p3d = Punto3D(3, 4, 5)

x = p3d.get_x()
y = p3d.get_y()
z = p3d.get_z()

print(f"Coordenadas del punto en 3D: ({x}, {y}, {z})")
