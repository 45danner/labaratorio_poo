"""Queremos representar con programación orientada a objetos, un aula con estudiantes y un profesor.
Tanto de los estudiantes como de los profesores necesitamos saber su nombre, edad y sexo. De los estudiantes, queremos saber también su calificación actual (entre 0 y 10) y del profesor que materia da.
Las materias disponibles son matemáticas, filosofía y física.
Los estudiantes tendrán un 50% de hacer novillos, por lo que si hacen novillos no van a clase pero aunque no vayan quedara registrado en el aula (como que cada uno tiene su sitio).
El profesor tiene un 20% de no encontrarse disponible (reuniones,
baja, etc.)
Las dos operaciones anteriores deben llamarse igual en Estudiante y Profesor (polimorfismo)
El aula debe tener un identificador numérico, el número máximo de estudiantes y para que esta destinada (matemáticas, filosofía o física). Piensa que más atributos necesita.
Un aula para que se pueda dar clase necesita que el profesor esté disponible, que el profesor de la materia correspondiente en el aula correspondiente (un profesor de filosofía no puede dar en un aula de matemáticas) y que haya más del 50% de alumnos.
El objetivo es crear un aula de alumnos y un profesor y determinar si puede darse clase, teniendo en cuenta las condiciones antes dichas.
Si se puede dar clase mostrar cuantos alumnos y alumnas (por separado) están aprobados de momento (imaginad que les están entregando las notas)."""


import random

class Persona:
    def __init__(self, nombre, edad, sexo):
        self.nombre = nombre
        self.edad = edad
        self.sexo = sexo

class Estudiante(Persona):
    def __init__(self, nombre, edad, sexo, calificacion):
        super().__init__(nombre, edad, sexo)
        self.calificacion = calificacion
        self.asistencia = True if random.random() > 0.5 else False

class Profesor(Persona):
    def __init__(self, nombre, edad, sexo, materia):
        super().__init__(nombre, edad, sexo)
        self.materia = materia
        self.disponible = True if random.random() > 0.2 else False

class Aula:
    def __init__(self, id_aula, max_estudiantes, materia):
        self.id_aula = id_aula
        self.max_estudiantes = max_estudiantes
        self.materia = materia
        self.estudiantes = []
        self.profesor = None
    
    def agregar_estudiante(self, estudiante):
        if len(self.estudiantes) < self.max_estudiantes:
            self.estudiantes.append(estudiante)
            return True
        else:
            return False
    
    def set_profesor(self, profesor):
        if profesor.materia == self.materia:
            self.profesor = profesor
            return True
        else:
            return False
    
    def puede_dar_clase(self):
        if self.profesor is not None and self.profesor.disponible and len(self.estudiantes) > self.max_estudiantes/2:
            return True
        else:
            return False
    
    def contar_aprobados(self):
        aprobados_hombres = 0
        aprobados_mujeres = 0
        for estudiante in self.estudiantes:
            if estudiante.calificacion >= 5:
                if estudiante.sexo == "Hombre":
                    aprobados_hombres += 1
                else:
                    aprobados_mujeres += 1
        return aprobados_hombres, aprobados_mujeres

aula = Aula(1, 20, "matemáticas")

profesor = Profesor("Juan", 35, "Hombre", "matemáticas")

estudiante1 = Estudiante("Ana", 18, "Mujer", 8)
estudiante2 = Estudiante("Pedro", 19, "Hombre", 4)
estudiante3 = Estudiante("María", 20, "Mujer", 6)
estudiante4 = Estudiante("Juan", 18, "Hombre", 7)

aula.agregar_estudiante(estudiante1)
aula.agregar_estudiante(estudiante2)
aula.agregar_estudiante(estudiante3)
aula.agregar_estudiante(estudiante4)

aula.set_profesor(profesor)

if aula.puede_dar_clase():
    print("Se puede dar clase")
    aprobados_hombres, aprobados_mujeres = aula.contar_aprobados()
    print("Número de hombres aprobados:", aprobados_hombres)
    print("Número de mujeres aprobadas:", aprobados_mujeres)
else:
    print("No se puede dar clase")
