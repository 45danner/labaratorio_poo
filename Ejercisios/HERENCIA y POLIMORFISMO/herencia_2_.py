"""Construir una clase final Math2 que amplíe las declaraciones de métodos estáticos de la clase
Math y que incluya funciones que devuelvan, respectivamente, el máximo, el mínimo, 
la sumatoria, la media aritmética y la media geométrica
de un array de números reales dado como parámetro."""
import math

class Math2:
    @staticmethod
    def max(arr):
        return max(arr)

    @staticmethod
    def min(arr):
        return min(arr)

    @staticmethod
    def sum(arr):
        return sum(arr)

    @staticmethod
    def mean(arr):
        return sum(arr) / len(arr)

    @staticmethod
    def geometric_mean(arr):
        product = 1
        for num in arr:
            product *= num
        return math.pow(product, 1 / len(arr))

# Ejemplo de uso
arr = [1, 2, 3, 4, 5]
print(Math2.max(arr))
print(Math2.min(arr))
print(Math2.sum(arr))
print(Math2.mean(arr))
print(Math2.geometric_mean(arr))
