class NumberList:
    def __init__(self):
        self.numbers = []

    def add_number(self, number):
        if isinstance(number, (int, float)):
            self.numbers.append(number)
        else:
            raise ValueError("El número debe ser de tipo int o float")

    def print_numbers(self):
        for number in self.numbers:
            print(number)
num_list = NumberList()
num_list.add_number(10)
num_list.add_number(3.14)
num_list.add_number(2.71828)
num_list.add_number(4)
num_list.print_numbers()
