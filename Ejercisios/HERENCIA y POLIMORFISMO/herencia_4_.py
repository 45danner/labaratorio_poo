"""
Un banco requiere controlar las cuentas de sus clientes y para ello las clasifica en dos: cheques y ahorros.
Todas las cuentas del banco tienen los siguientes datos:
• Número de cuenta (entero).
• Nombre del cliente (cadena).
• Saldo (numérico real).
Además se realizan las siguientes operaciones con ellas:
• Consultar datos: A través de sus propiedades.
• Depositar: Incrementa el saldo con la cantidad de dinero que se deposita.
• Retirar: Antes de hacer el retiro, se debe verificar la suficiencia de saldo y en caso de aprobarlo, se disminuye el saldo.
Las cuentas de ahorros presentan las siguientes características:
• Fecha de vencimiento.
• Porcentaje de interés mensual.
• Método para depositar los intereses el primer día de cada mes.
• Solamente se puede retirar dinero el día de la fecha de vencimiento.
Las cuentas de cheques presentan las siguientes características:
• Comisión por uso de chequera.
• Comisión por emisión de cheques con saldo insuficiente, la cual se descuenta directamente del saldo.
Implemente dicho sistema empleando polimorfismo.
Implemente la sobrescritura del método para mostrar los datos de cada tipo de objeto.
"""
from datetime import date, datetime

class Cuenta:
    def __init__(self, num_cuenta, nombre_cliente, saldo):
        self.num_cuenta = num_cuenta
        self.nombre_cliente = nombre_cliente
        self.saldo = saldo

    def consultar_datos(self):
        print(f"Número de cuenta: {self.num_cuenta}")
        print(f"Nombre del cliente: {self.nombre_cliente}")
        print(f"Saldo: {self.saldo}")

    def depositar(self, cantidad):
        self.saldo += cantidad

    def retirar(self, cantidad):
        if self.saldo >= cantidad:
            self.saldo -= cantidad
            return True
        else:
            return False

    def mostrar_datos(self):
        self.consultar_datos()

class CuentaAhorros(Cuenta):
    def __init__(self, num_cuenta, nombre_cliente, saldo, fecha_vencimiento, interes_mensual):
        super().__init__(num_cuenta, nombre_cliente, saldo)
        self.fecha_vencimiento = fecha_vencimiento
        self.interes_mensual = interes_mensual

    def depositar_intereses(self):
        intereses = self.saldo * self.interes_mensual
        self.depositar(intereses)
        print(f"Se han depositado {intereses} de intereses en la cuenta de ahorros.")

    def retirar(self, cantidad):
        if self.saldo >= cantidad and self.fecha_vencimiento == datetime.now().date():
            self.saldo -= cantidad
            return True
        else:
            return False

    def mostrar_datos(self):
        super().consultar_datos()
        print(f"Fecha de vencimiento: {self.fecha_vencimiento}")
        print(f"Porcentaje de interés mensual: {self.interes_mensual}")

class CuentaCheques(Cuenta):
    def __init__(self, num_cuenta, nombre_cliente, saldo, comision_chequera, comision_saldo_insuficiente):
        super().__init__(num_cuenta, nombre_cliente, saldo)
        self.comision_chequera = comision_chequera
        self.comision_saldo_insuficiente = comision_saldo_insuficiente

    def retirar(self, cantidad):
        if self.saldo >= cantidad + self.comision_chequera:
            self.saldo -= cantidad + self.comision_chequera
            return True
        else:
            self.saldo -= self.comision_saldo_insuficiente
            return False

    def mostrar_datos(self):
        super().consultar_datos()
        print(f"Comisión por uso de chequera: {self.comision_chequera}")
        print(f"Comisión por emisión de cheques con saldo insuficiente: {self.comision_saldo_insuficiente}")

cuenta_ahorro = CuentaAhorros(123456, "Danner Perez", 1000000, date(2023, 4, 1), 0.01)
cuenta_cheque = CuentaCheques(654321, "Hilda Perez", 5000, 50, 100)

cuenta_ahorro.mostrar_datos()
cuenta_cheque.mostrar_datos()

cuenta_ahorro.depositar_intereses()
cuenta_ahorro.retirar(500)
cuenta_cheque.retirar(500)
cuenta_cheque.retirar(5000)

cuenta_ahorro.mostrar_datos()
cuenta_cheque.mostrar_datos()