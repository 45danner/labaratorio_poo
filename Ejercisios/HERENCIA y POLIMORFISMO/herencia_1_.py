#Construir una clase Factura que descienda de la clase Precio
#y que incluya dos atributos específicos llamados emisor y 
#cliente y, al menos, un método llamado imprimirFactura.

class Precio:
    def __init__(self, valor):
        self.valor = valor

    def __str__(self):
        return f"${self.valor:.2f}"

class Factura(Precio):
    def __init__(self, valor, emisor, cliente):
        super().__init__(valor)
        self.emisor = emisor
        self.cliente = cliente

    def imprimirFactura(self):
        print("Factura:")
        print(f"Emisor: {self.emisor}")
        print(f"Cliente: {self.cliente}")
        print(f"Total: {self}")
        
factura = Factura(100.50, "Mi empresa", "Cliente A")
factura.imprimirFactura()