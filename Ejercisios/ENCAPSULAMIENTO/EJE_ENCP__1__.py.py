"""Crear una clase llamada Persona. Sus atributos son:
nombre, edad y dni.
Construye los siguientes métodos para la clase:
Un constructor, donde los datos pueden estar vacíos.
Los setters y getters para cada uno de los atributos. Hay que validar las entradas de datos.
mostrar(): muestra los datos de la persona.
es_mayor_de_edad(): Devuelve un valor lógico indicando si es mayor de edad."""

class Persona:
    def __init__(self, nombre="", edad=0, dni=""):
        self.nombre = nombre
        self.edad = edad
        self.dni = dni

    def get_nombre(self):
        return self.nombre

    def set_nombre(self, nombre):
        self.nombre = nombre

    def get_edad(self):
        return self.edad

    def set_edad(self, edad):
        if edad >= 0:
            self.edad = edad
        else:
            print("La edad no puede ser negativa.")

    def get_dni(self):
        return self.dni

    def set_dni(self, dni):
        self.dni = dni

    def mostrar(self):
        print("Nombre:", self.nombre)
        print("Edad:", self.edad)
        print("DNI:", self.dni)

    def es_mayor_de_edad(self):
        return self.edad >= 18
    
p = Persona()
p.set_nombre("Juan")
p.set_edad(25)
p.set_dni("12345678A")
p.mostrar()
print("Es mayor de edad:", p.es_mayor_de_edad())
