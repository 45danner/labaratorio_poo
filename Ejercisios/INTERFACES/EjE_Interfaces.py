"""Crear un programa que permita calcular el arear de un cuadrado,
rectángulo, circulo y elipse usando interfaces. El programa debe contar con un menú,
para el cual se debe crear una clases abstracta."""
import math
from abc import ABC, abstractmethod

class Figura(ABC):
    @abstractmethod
    def area(self):
        pass

class Cuadrado(Figura):
    def __init__(self, lado):
        self.lado = lado
    
    def area(self):
        return self.lado ** 2

class Rectangulo(Figura):
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura
    
    def area(self):
        return self.base * self.altura

class Circulo(Figura):
    def __init__(self, radio):
        self.radio = radio
    
    def area(self):
        return math.pi * self.radio ** 2

class Elipse(Figura):
    def __init__(self, semieje_mayor, semieje_menor):
        self.semieje_mayor = semieje_mayor
        self.semieje_menor = semieje_menor
    
    def area(self):
        return math.pi * self.semieje_mayor * self.semieje_menor

while True:
    print("Selecciona una opción:")
    print("1. Calcular área de cuadrado")
    print("2. Calcular área de rectángulo")
    print("3. Calcular área de círculo")
    print("4. Calcular área de elipse")
    print("5. Salir")

    opcion = input("Introduce el número de opción: ")

    if opcion == "1":
        lado = float(input("Introduce la medida del lado: "))
        cuadrado = Cuadrado(lado)
        print("El área del cuadrado es:", cuadrado.area())

    elif opcion == "2":
        base = float(input("Introduce la medida de la base: "))
        altura = float(input("Introduce la medida de la altura: "))
        rectangulo = Rectangulo(base, altura)
        print("El área del rectángulo es:", rectangulo.area())

    elif opcion == "3":
        radio = float(input("Introduce la medida del radio: "))
        circulo = Circulo(radio)
        print("El área del círculo es:", circulo.area())

    elif opcion == "4":
        semieje_mayor = float(input("Introduce la medida del semieje mayor: "))
        semieje_menor = float(input("Introduce la medida del semieje menor: "))
        elipse = Elipse(semieje_mayor, semieje_menor)
        print("El área de la elipse es:", elipse.area())

    elif opcion == "5":
        print("Saliendo del programa...")
        break

    else:
        print("Opción inválida")
