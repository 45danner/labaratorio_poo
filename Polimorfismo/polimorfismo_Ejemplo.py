#Ejemplo de polimorfismo 
class Figura:
    def area(self):
        pass

class Cuadrado(Figura):
    def __init__(self, lado):
        self.lado = lado

    def area(self):
        return self.lado ** 2

class Circulo(Figura):
    def __init__(self, radio):
        self.radio = radio

    def area(self):
        return 3.14159 * self.radio ** 2

# Crear objetos de cada clase
figura1 = Cuadrado(5)
figura2 = Circulo(3)

# Llamar al método área en cada objeto
print("Área del cuadrado:", figura1.area())
print("Área del círculo:", figura2.area())
