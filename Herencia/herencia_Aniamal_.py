class Animal:
    def __init__(self, nombre, especie):
        self.nombre = nombre
        self.especie = especie
    
    def hacer_sonido(self):
        pass

class Perro(Animal):
    def hacer_sonido(self):
        return "Guau"

class Gato(Animal):
    def hacer_sonido(self):
        return "Miau"


animal_generico = Animal("Animal Genérico", "Desconocida")
perro = Perro("Fido", "Canino")
gato = Gato("Misi", "Felino")

print(animal_generico.hacer_sonido())  # No se muestra ningún sonido, ya que no se ha sobrescrito el método hacer_sonido()

print(perro.hacer_sonido())  # Se muestra "Guau", que es el sonido que devuelve la clase Perro

print(gato.hacer_sonido())  # Se muestra "Miau", que es el sonido que devuelve la clase Gato
